from census_maps.csv2dbf import merge_csv_into_dbf
from census_maps.shp_dbf_to_svg import render_shapefile, get_dbf_records_iter
from census_maps.jenks import get_jenks_colors

###This is what you will need to do if your dbf doesn't already contain the attribute column you are building the choropleth with. Currently not guaranteed to work
#merged_dbf = 'tl_2010_us_county10/county_income.dbf'
#merge_csv_into_dbf('tl_2010_us_county10/tl_2010_us_county10.dbf','output.csv',merged_dbf,dbf_id_col='GEOID10',csv_id_col='attribute')

#Here you are finding the dbf column headers and then picking which column header you want to use to build the dbf
field_names, iter_records = get_dbf_records_iter("tl_2010_us_county10/tl_2010_us_county10.dbf")
print field_names
f_ind = field_names.index('LSAD10')

#Choosing the number of color divisions 
num_groups = 8
color_group = 'Yl0rBr'

#get_jenks_colors is a module that picks an aethetically pleasing color range 
colors = get_jenks_colors(iter_records,f_ind,num_groups,color_group)

#uses the shapefile to output an svg and yaml file describing its placement on sluice
render_shapefile('tl_2010_us_county10/tl_2010_us_county10.shp','us-county-incomes.svg',colors=colors)
